**Requirements:**
 - Composer
 - PHP At least version 7.4

To run tests, run `php vendor/bin/phpunit tests/`
To execute the app with the tester file run `php run_me.php`
