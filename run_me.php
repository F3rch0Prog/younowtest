<?php
/**
 * Fernando Paz <fspazfranco@gmail.com>.
 */
declare(strict_types=1);

use Fernandop\YouNowTest\Queue\QueueManager;

// Allow only CLI execution
if ('cli' !== PHP_SAPI) {
    die('Only run it form CLI.'.PHP_EOL);
}

// Add dependencies
include_once(__DIR__.DIRECTORY_SEPARATOR.'src/Queue/QueueManager.php');
// OR
// include_once(__DIR__.DIRECTORY_SEPARATOR.'vendor/autoload.php');

// Open file
$actionsFilename = __DIR__.DIRECTORY_SEPARATOR.'resources/actions.txt';
$handle          = fopen($actionsFilename, 'rb');
if (!$handle) {
    die('Actions file not found in '.$actionsFilename.PHP_EOL);
}

// Create queue manager
$manager = new QueueManager();

// Read file
while (($line = fgets($handle)) !== false) {
    if (!trim($line)) {
        continue;
    }
    // Parse line
    $line      = explode(',', trim($line));
    $action    = ucwords(str_replace('_', ' by ', strtolower(array_shift($line))));
    $action    = str_replace(' ', '', $action);
    $action[0] = strtolower($action[0]);
    $args      = array_map('intval', $line);

    try {
        // Notify running action
        echo sprintf(
            'Running action "%s" with arguments "%s"',
            $action,
            implode(', ', $args)
        );
        echo PHP_EOL;
        // Run action
        $result = call_user_func_array([$manager, $action], $args);
        // Output result
        echo sprintf(
            'Returned: %s',
            (true === $result ? 'TRUE' : (false === $result ? 'FALSE' : $result))
        );
        echo PHP_EOL;
    } catch (Throwable $e) {
        echo sprintf(
            'Unable to run last action. Error: %s',
            $e->getMessage()
        );
        echo PHP_EOL;
    }
}

fclose($handle);
