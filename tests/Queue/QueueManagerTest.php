<?php
/**
 * Fernando Paz <fspazfranco@gmail.com>.
 */

namespace Fernando\YouNowTest\Test\Queue;

use Fernandop\YouNowTest\Queue\QueueManager;
use PHPUnit\Framework\TestCase;
use ReflectionClass;

/**
 * Tester for queue manager
 */
class QueueManagerTest extends TestCase
{
    /**
     * Test adding a user in the queue
     */
    public function testAdd(): void
    {
        $manager = new QueueManager();

        // Non-existent user in queue
        $userId   = 1;
        $position = $manager->add($userId);
        self::assertNotFalse($position);
        self::assertSame(1, $position);

        // Existent user in queue
        $position = $manager->add($userId);
        self::assertFalse($position);

        // Non-existent user in queue
        $userId   = 2;
        $position = $manager->add($userId);
        self::assertNotFalse($position);
        self::assertSame(2, $position);
    }

    /**
     * Test remove by user
     */
    public function testRemoveByUser(): void
    {
        $manager = new QueueManager();

        // Non-existent user in queue
        $userId = 1;
        $result = $manager->removeByUser($userId);
        self::assertFalse($result);

        // Add to queue
        $manager->add($userId);

        // Existent user in queue
        $result = $manager->removeByUser($userId);
        self::assertTrue($result);

        // Non-existent user in queue
        $result = $manager->removeByUser($userId);
        self::assertFalse($result);
    }

    /**
     * Test remove user at position
     */
    public function testRemoveByPosition(): void
    {
        $manager = new QueueManager();

        // Non-existent user in queue
        $userId = 1;
        $result = $manager->removeByPosition($userId);
        self::assertFalse($result);

        // Add to queue
        $manager->add($userId);

        // Existent user in queue
        $result = $manager->removeByUser(1);
        self::assertTrue($result);

        // Non-existent user in queue
        $result = $manager->removeByUser($userId);
        self::assertFalse($result);

        // Add multiple to queue
        $manager->add(1);
        $manager->add(5);
        $manager->add(2);

        $result = $manager->removeByUser(5);
        self::assertTrue($result);
    }

    /**
     * Test move user to a new position
     */
    public function testMove(): void
    {
        $manager = new QueueManager();

        // Empty queue
        $result = $manager->move(1, 2);
        self::assertFalse($result);

        // Add multiple to queue
        $manager->add(1); // 1
        $manager->add(3); // 2
        $manager->add(6); // 3
        $manager->add(4); // 4
        $manager->add(9); // 5

        // Move 4th user to 2nd
        $result = $manager->move(4, 2);
        self::assertTrue($result);

        // Non-existent initial position in queue
        $result = $manager->move(6, 1);
        self::assertFalse($result);

        // Non-existent final position in queue
        $result = $manager->move(5, 6);
        self::assertFalse($result);
    }

    /**
     * Test swapping user ids
     */
    public function testSwap(): void
    {
        $manager = new QueueManager();

        // Empty queue
        $result = $manager->swap(1, 2);
        self::assertFalse($result);

        // Add multiple to queue
        $manager->add(1); // 1
        $manager->add(3); // 2
        $manager->add(6); // 3
        $manager->add(4); // 4
        $manager->add(9); // 5

        // Move 4th user to 2nd
        $result = $manager->swap(4, 2);
        self::assertTrue($result);

        // Non-existent initial position in queue
        $result = $manager->swap(6, 1);
        self::assertFalse($result);

        // Non-existent final position in queue
        $result = $manager->swap(5, 6);
        self::assertFalse($result);
    }

    /**
     * Test reversing order of queue
     */
    public function testReverse(): void
    {
        $manager = new QueueManager();

        $refClass  = new ReflectionClass($manager);
        $queueProp = $refClass->getProperty('queue');
        $queueProp->setAccessible(true);

        self::assertEmpty($queueProp->getValue($manager));
        self::assertIsArray($queueProp->getValue($manager));

        // Add multiple to queue
        $manager->add(1); // 1
        $manager->add(3); // 2
        $manager->add(6); // 3
        $manager->add(4); // 4
        $manager->add(9); // 5

        self::assertSame([1, 3, 6, 4, 9], $queueProp->getValue($manager));
        self::assertTrue($manager->reverse());
        self::assertSame([9, 4, 6, 3, 1], $queueProp->getValue($manager));
    }

    /**
     * Test printing queue
     */
    public function testPrint(): void
    {
        $manager = new QueueManager();

        // Add multiple to queue
        $manager->add(10); // 1
        $manager->add(45); // 2
        $manager->add(1);  // 3
        $manager->add(20); // 4
        $manager->add(40); // 5

        ob_start();
        self::assertTrue($manager->print());
        $printed = ob_get_clean();

        $pieces = explode(PHP_EOL, trim($printed));
        self::assertCount(5, $pieces);
        self::assertSame(
            [
                '1:10',
                '2:45',
                '3:1',
                '4:20',
                '5:40',
            ],
            $pieces
        );
    }
}
