<?php
/**
 * Fernando Paz <fspazfranco@gmail.com>.
 */
declare(strict_types=1);

namespace Fernandop\YouNowTest\Queue;

/**
 * Queue manager
 */
class QueueManager
{
    /**
     * @var array
     */
    private array $queue;

    /**
     * QueueManager constructor
     */
    public function __construct()
    {
        $this->queue = [];
    }

    /**
     * Add a user to the queue
     *
     * @param int $userId User id
     *
     * @return int|false Position if non-existent user id in the queue or false
     */
    public function add(int $userId)
    {
        if (!$userId) {
            // Invalid user id
            return false;
        }

        // If by some reason multiple requests get here for the same user id at the same time
        // sleep randomly to decrease possibility of adding the user multiple times
        // NOTE: if this was a real queue it will implement a single entrypoint with locking
        usleep(random_int(5, 20));

        // Test if user in queue
        if (in_array($userId, $this->queue, true)) {
            return false;
        }

        $this->queue[] = $userId;

        return count($this->queue);
    }

    /**
     * Remove a user from the queue
     *
     * @param int $userId
     *
     * @return bool True if removed false otherwise or if non-existent user
     */
    public function removeByUser(int $userId): bool
    {
        if (!$userId) {
            // Invalid user id
            return false;
        }

        // To dequeue a user no need to wait
        if (false === ($index = array_search($userId, $this->queue, true))) {
            return false;
        }

        array_splice($this->queue, $index, 1);

        return true;
    }

    /**
     * Remove user at position
     *
     * @param int $position Starts at 1
     *
     * @return bool True if removed false otherwise or if invalid position
     */
    public function removeByPosition(int $position): bool
    {
        if (!$this->validatePositions($position)) {
            return false;
        }

        array_splice($this->queue, $position - 1, 1);

        return true;
    }

    /**
     * Move user to a new position
     *
     * @param int $initial User from position
     * @param int $final   To final position
     *
     * @return bool True if moved false otherwise or if invalid position(s)
     */
    public function move(int $initial, int $final): bool
    {
        if (!$this->validatePositions($initial, $final)) {
            return false;
        }

        $userId = array_splice($this->queue, $initial - 1, 1);
        array_splice($this->queue, $final - 1, 0, $userId);

        return true;
    }

    /**
     * Swap users between positions
     *
     * @param int $positionOne First position
     * @param int $positionTwo Second position
     *
     * @return bool True if moved false otherwise or if invalid position(s)
     */
    public function swap(int $positionOne, int $positionTwo): bool
    {
        if (!$this->validatePositions($positionOne, $positionTwo)) {
            return false;
        }

        $atPos1                        = $this->queue[$positionOne - 1];
        $this->queue[$positionOne - 1] = $this->queue[$positionTwo - 1];
        $this->queue[$positionTwo - 1] = $atPos1;

        return true;
    }

    /**
     * Reverse the queue
     *
     * @return bool
     */
    public function reverse(): bool
    {
        $this->queue = array_reverse($this->queue);

        return true;
    }

    /**
     * Print current queue.
     * Format {position}:{userId}
     *
     * @return bool
     */
    public function print(): bool
    {
        foreach ($this->queue as $index => $userId) {
            echo ($index + 1).':'.$userId.PHP_EOL;
        }

        return true;
    }

    /**
     * Validate positions
     *
     * @param int ...$positions
     *
     * @return bool
     */
    private function validatePositions(int ...$positions): bool
    {
        foreach ($positions as $position) {
            if ($position < 1 || !isset($this->queue[$position - 1])) {
                return false;
            }
        }

        return true;
    }
}
